#importing probably more than what is needed
import numpy as np
import matplotlib.pyplot as plt
from skimage import data
from skimage.feature import match_template
from skimage import feature
from skimage.color import rgb2gray
from skimage import io
from skimage import data, filters
from skimage.color import rgb2gray
from skimage.transform import rotate

#this imports the main image
image = io.imread('samples/sample.tif')

#changing it to grayscale:
image = rgb2gray(image)

#create the template image of a single triangle using a subset of the parent image
#triangle = image[798:893,1437:1525 ]
triangle = io.imread('samples/single.tif')
triangle = rgb2gray(triangle)

#angle = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
angle = range(0,120,1)
for i in angle:
#Rotation is for experimenting. I determined the orientation of the template image matters.
	#triangle = image[798:893,1437:1525 ]
	triangle = io.imread('samples/single.tif')
	triangle = rgb2gray(triangle)
	triangle = rotate(triangle, i, resize = False)

#this performs the actual template matching and outputs an array called 'result'
	result = match_template(image, triangle)
#	print image.shape, result.shape
#	print type(image), type(result)
#	print result.min(), result.max()


#plot and save the new image
	plt.imshow(result)
	plt.savefig('output/rotation test 5/{angle}.png'.format(angle = i))
	io.imsave('output/rotation test 5/template{angle}.png'.format(angle = i),triangle)

