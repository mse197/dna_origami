# README #

To install the scikit-image package for handling pictures, do:

```
#!bash
$ pip install --install-option="--prefix=${HOME}/software" scikit-image
```

# Program Overview #

This piece of code provides a method to import an atomic force microscopy (AFM) image.

Once imported, the code attempts to remove undesired/unneeded "noise" from the image.

The methods used for "noise" removal follow a sequence of steps:

1. Load the modules needed for computer vision

2. Create an output directory if it does not exists (storage of processed images)

3. Convert the image to gray scale (3-D array conversion to 2-D array)

4. Apply a Gaussian filter (Blur image to reduce "noise")

5. Set a threshold (Convert the pixels to Black or White)

6. Utilize a disk to create a mask (removes unwanted pixels in image)

7. Invert the image (Changes Black to White and White to Black, allows for counting)

8. Determine the surface density

9. Count the origami's present



# Program Requirements #

A number of modules are needed in order for this code to function correctly.  Installation instructions and files are typically found at each module's website

1. Numpy http://www.scipy.org/scipylib/download.html or  http://sourceforge.net/projects/numpy/files/
 
2. Scipy http://www.scipy.org/scipylib/download.html or http://sourceforge.net/projects/scipy/files/

3. Scikit-Image http://scikit-image.org/download.html



# Preparation to run the program #

The user is required to have an image available for the program to import.  

This image will need to be in a folder named "samples", as this is where the code looks.

Once this folder is created an image with the name "full.tif" must be placed there, again this is what the code calls on.


# Running the code #

Make sure you are in the directory that contains the "samples" folder

Typing 'python imagearray.py' at the bash interface will initiate the program

Upon completion (depending on machine speed) the code will output 5 images as follows:

1. '0.png' - this is the gray scale image

2. '1.png' - this is the Gaussian filtered image

3. '2.png' - this is the image after the mask has been applied

4. '3mask.png' - this is the image that has had 'salt' removed and holes filled

5. '4centers.png' - this is the inverted image, it represents only the centers of the dna triangles

These files will be found in the output folder initially create by the program in the current working directory


# Areas for improvement #

Currently, this code works on a provided image with very high resolution.

Most images are not at this resolution due to time involved to capture.

* Allow the user to input the name of the file to work with

* Increase the 'robustness' of the code to work with lower resolution images

* Allow the user to control the parameters (Gaussian filter, threshold, disk size)

* Output the images to the computer screen as they are produced.