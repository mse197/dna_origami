# Imports modules needed to perform image manipulation
import numpy
import scipy
from scipy import misc
from scipy import ndimage
from skimage import segmentation, measure
from skimage import morphology
from skimage.morphology import watershed
from skimage.feature import peak_local_max

# Import entire matplotlib module for ease
from pylab import *

# Import entire Python Imaging Library module for ease
from PIL import *

# Opens the file and assigns it to a variable
image =array(Image.open('samples/full.tif').convert('L'))
image = image[0:500,0:500]

# Applies a Gaussian filter with sigma = 11
# Assings two variable with the same Gaussian application to visualize
# What the Gaussian and the mask do to the original image
ga = ndimage.gaussian_filter(image,9)
ga1 =  ndimage.gaussian_filter(image,9)

# Applies a mask to the filtered image to get a B&W final image
print ga1.min(), ga1.max()
ga1[ga1<75]=0
ga1[ga1>150]=1
print ga1.min(), ga1.max()
ga1[ga1>0]=255
print ga1.min(), ga1.max()

#Saves image after mask
misc.imsave('output/ga1-pre.png',ga1)

#Generates a distance to peak values in array saves image of peak values
distance = ndimage.distance_transform_edt(ga1)
misc.imsave('output/distance.png',distance)

#Finds local maximum of peaks and saves image
local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)), labels=ga1)
misc.imsave('output/local_max.png',local_maxi)

#Sums total number of maximums
print local_maxi.sum()


#Still working on the code below...

#markers = measure.label(local_maxi)
#labels_ws = watershed(-distance, markers, mask=ga1)
# Transform markers image so that 0-valued pixels are to
# be labelled, and -1-valued pixels represent background

#markers[~ga1] = -1
#labels_rw = segmentation.random_walker(ga1, markers)


#Saves the figure to a file and assigns a filename
#misc.imsave('output/gf_75_150_seg.png',ga1)



