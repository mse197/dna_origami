# Imports modules needed to perform image manipulation
import numpy
import scipy
from scipy import misc
from scipy import ndimage

# Import entire matplotlib module for ease
from pylab import *

# Import entire Python Imaging Library module for ease
from PIL import *

# Opens the file and assigns it to a variable
image =array(Image.open('samples/full.tif').convert('L'))

# Applies a Gaussian filter with sigma = 11
# Assings two variable with the same Gaussian application to visualize
# What the Gaussian and the mask do to the original image
ga = ndimage.gaussian_filter(image,11)
ga1 =  ndimage.gaussian_filter(image,11)

# Applies a mask to the filtered image to get a B&W final image
print ga1.min(), ga1.max()
ga1[ga1<75]=0
ga1[ga1>150]=1
print ga1.min(), ga1.max()
ga1[ga1>0]=255
print ga1.min(), ga1.max()

# Creates a set of 3 subplots to output the image
figure(figsize=(10,20))
subplot(231)
imshow(image)
subplot(232)
imshow(ga, cmap=gray())
subplot(233)
imshow(ga1, cmap=gray())


subplots_adjust(wspace=0.02, hspace=0.1, top=0.9, bottom=0, left=0,
                    right=1)

# Saves the figure to a file and assigns a filename
savefig('output/gf_75_150.jpg')
misc.imsave('output/gf_75_150.png',ga1)

# Outputs the image to the screen
	#This may not work on the BSU computers
#show()

