import numpy
import scipy
from scipy import misc
from scipy import ndimage
from pylab import *
from PIL import *

image =array(Image.open('zoom1.tif').convert('L'))
ga = ndimage.gaussian_filter(image,11)

fga=ndimage.gaussian_filter(ga,1)

alpha=30
sharp=ga + alpha * (ga-fga)


figure()
hist1=hist(ga.flatten(),128)
hist2=hist(sharp.flatten(),128)


show()
