#import module for plotting the file
from matplotlib import pyplot as plt

# Import modules for image processing
from skimage.color import rgb2gray
from skimage import io

# Initial code to read in the image
image = io.imread('samples/full.tif')

# Converts color image to grayscale
image = rgb2gray(image)

# Find the mean value of pixels and assign a vairable
m = image.mean()

# Assigns array valui of 1
image[image<0.4]=0
image[image>0.6]=0
image[image>0]=1.

io.imsave('output/example2.png',image)

