
#import module for plotting the file
from skimage.color import rgb2gray
from skimage import io, morphology, measure
from scipy import ndimage
import numpy as np
from skimage.draw import ellipse


from os import listdir
from os.path import isfile, join
files = [f for f in listdir('samples/') if isfile(join('samples/', f))]
#print files

for i in range(len(files)):
	print i, files[i], '\n'
fileindex = int(raw_input('type the number corresponding to the picture you wish to analyze and press enter: '))
filepath = files[fileindex]

# Converts color image to grayscale
image = io.imread('samples/{}'.format(filepath))
image = rgb2gray(image)
io.imsave('output/0.png',image)
image = ndimage.gaussian_filter(image,5)
io.imsave('output/1.png',image)

#Pretty decent thresholding
image[image<0.3]=False
image[image>0]=True
io.imsave('output/2.png',image)

#Now clean up "salt", fill in holes
sel = morphology.disk(4)
main_mask = morphology.opening(image,sel)
io.imsave('output/3mask.png',main_mask)
filled = main_mask.sum()
#inverts image
centers = np.zeros(main_mask.shape)
centers[main_mask==False]=True
labels,nlabels = ndimage.label(centers)
print "Initially,",nlabels, "clusters, including the background."
sizes = ndimage.sum(centers,labels,index=range(0,nlabels))
for i,s in enumerate(sizes): #only keep little clusters, the center triangles
    if s > 500.:
        centers[labels==i] = False
io.imsave('output/4centers.png',centers)

labels,nlabels = ndimage.label(centers)
print "There are approximately", nlabels, "nanostructures."
print "And on average,", int(filled/float(nlabels)), "pixels per nanostructure."


#store all outputs of regionprops in variable 'p'
p=measure.regionprops(labels)

#Eccentricity calclates the shape of ellipse with an eccentricity value of 0 being a circle
#extract eccentricity of center regions in order to filter out false positives
eccentricity = []
for i in range(len(p)):
	eccentricity.append(p[i]['eccentricity'])
mean = sum(eccentricity)/len(eccentricity)
#print "the mean eccentricity is:", mean 



#extract the coordinates of the triangle centers as well as their orientations, then save an image with just the centers as single white pixels 
#simultaneously filter out false positives using eccentricity, 'mean + 0.3' seems to be optimal value
centroid = []
orientations = []

numtris = 0
funmask = np.zeros(labels.shape)
for i in range(len(p)):
	if p[i]['eccentricity'] < mean + 0.3:
        	centroid.append([int(p[i]['centroid'][0]),int(p[i]['centroid'][1])])
		eccentricity.append(p[i]['eccentricity'])
		orientations.append(p[i]['orientation'])
		funmask[centroid[-1][0],centroid[-1][1]] = True
		numtris = numtris + 1
io.imsave('output/5just_centroids.png',funmask)

print 'the number of triangles based on eccentricity is:', numtris

# output an image to label which tringles were counted by placing an ellipse centered on the centroids from the previous step. this is just for better visibility
centerdots = np.zeros(labels.shape)
for i in range(len(centroid)):
	rr,cc = ellipse(centroid[i][0],centroid[i][1],2,2)
	placeholder = np.zeros(labels.shape)
	placeholder[rr,cc] = 1
	centerdots = centerdots + placeholder
centermask =  centerdots>1
centerdots[centermask] = 1 
io.imsave('output/6centerdots.png',centerdots)

#Overlay the ellipse centers image over the original image to form an image marking where a triangle was counted
original = io.imread('samples/{}'.format(filepath))

original = rgb2gray(original)
final = original + centerdots
finalmask = final > 1
final[finalmask] = 1
io.imsave('output/6marked.png',final)

print "Average orientation of the tiles in radians is", (sum(orientations)/len(orientations))
