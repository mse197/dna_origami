# Import module for plotting the file
from matplotlib import pyplot as plt

# Import modules for image processing
from skimage.color import rgb2gray
from skimage import io

# Initial code to read in the image
image = io.imread('samples/full.tif')

# Converts color image to grayscale
image = rgb2gray(image)

# Find the mean value of pixels and assign a vairable
m = image.mean()

# Set a minimum pixel value to black
image[image<m/1.5]=0.

# Set a maximum pixel value to black
image[image>m*3.0]=0.

# Save the manipulated  image with a new filename
io.imsave('output/test.png',image)

