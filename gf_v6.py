# Imports modules needed to perform image manipulation
import scipy
from scipy import misc
from scipy import ndimage
from skimage import data, io, feature, filters
from skimage.color import rgb2gray

# Import entire matplotlib module for ease
from pylab import *

# Import entire Python Imaging Library module for ease
from PIL import *

# Import entire numpy module for ease
from numpy import *

# Opens the file and assigns it to a variable
image =io.imread('samples/full.tif')

img = rgb2gray(image)

ga = ndimage.gaussian_filter(img,11)
ga1 = ndimage.gaussian_filter(img,11)


# Initial mask of Gaussian filtered image
ga1[ga1<.3]=0
ga1[ga1>.75]=1
ga1[ga1>.1]=1


# Removal of mask from original iamge
ga2 = ga1*(img)

# Set debris to black
ga2[ga2>0.8]=0

# Set 'mask1' variable to mean pixel value
mask1=ga2>ga2.mean()

# Set mean value to white
ga2[mask1]=1

# Set plot size
figure(figsize=(16,5))

# Set up 1x4 plot
subplot(141)
imshow(img)
axis('off')
title('Original')

subplot(142)
imshow(ga, cmap=gray())
axis('off')
title('Gaussian')

subplot(143)
imshow(ga1, cmap=gray())
axis('off')
title('Mask')

subplot(144)
imshow(ga2, cmap=gray())
axis('off')
title('Orig - Mask')

subplots_adjust(wspace=0.02, hspace=0.02, top=0.9, bottom=0, left=0, right=1)

# Save all images for visual analysis
misc.imsave('output/gf_v6_image.jpg',img)
misc.imsave('output/gf_v6_gaussian.jpg',ga)
misc.imsave('output/gf_v6_mask.jpg',ga1)
misc.imsave('output/gf_v6_final.jpg',ga2)


# Output the images on screen
show()
