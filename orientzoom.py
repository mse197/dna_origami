#import module for plotting the file
from skimage.color import rgb2gray
from skimage import io, morphology, measure
from scipy import ndimage
import numpy as np
import os

if not os.path.exists('output1'):
	os.makedirs('output1')


# Converts color image to grayscale
image = io.imread('samples/zoom1.tif')
image = rgb2gray(image)
io.imsave('output1/0.png',image)
image = ndimage.gaussian_filter(image,5)
io.imsave('output1/1.png',image)

#Pretty decent thresholding
image[image<0.3]=False
image[image>0]=True
io.imsave('output1/2.png',image)

#Now clean up "salt", fill in holes
sel = morphology.disk(4)
main_mask = morphology.opening(image,sel)
io.imsave('output1/3mask.png',main_mask)
filled = main_mask.sum()
#inverts image
centers = np.zeros(main_mask.shape)
centers[main_mask==False]=True
labels,nlabels = ndimage.label(centers)
print "Initially,",nlabels, "clusters, including the background."
sizes = ndimage.sum(centers,labels,index=range(0,nlabels))
for i,s in enumerate(sizes): #only keep little clusters, the center triangles
    if s > 500.:
        centers[labels==i] = False
io.imsave('output1/4centers.png',centers)

labels,nlabels = ndimage.label(centers)
print "There are approximately", nlabels, "nanostructures."
print "And on average,", int(filled/float(nlabels)), "pixels per nanostructure."

#store all outputs of regionprops in variable 'p'
p=measure.regionprops(labels)
#prints orientation of specified object

#print "The orientation from x-axis of all tiles is", p[0]['orientation'], "radians."
o = []
for i in p:
	o.append(i['orientation'])

print "The orientation from x-axis of all tiles is", o[:], "radians."

