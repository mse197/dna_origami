# Import modules for program to run
from skimage.color import rgb2gray
from skimage import io, morphology
from scipy import ndimage
import numpy as np
import os

# Logic to create the output directory if it does not exist
if not os.path.exists('output'):
	os.makedirs('output')


# Converts color image to grayscale
image = io.imread('samples/full.tif')
image = rgb2gray(image)
io.imsave('output/0.png',image)
image = ndimage.gaussian_filter(image,5.414)
io.imsave('output/1.png',image)

# Thresholding
image[image<0.25]=False
image[image>0]=True
io.imsave('output/2.png',image)

# Clean up "salt", fill in holes
sel = morphology.disk(4)
main_mask = morphology.opening(image,sel)
io.imsave('output/3mask.png',main_mask)

# Count pixels to get surface area.
filled = main_mask.sum()
coverage = float(filled)/(main_mask.shape[0]*main_mask.shape[1])
print
print int(filled), "filled pixels.", "Percentage covered:", (coverage*100)," \n"

# Invert image to allow for counting
centers = np.zeros(main_mask.shape)
centers[main_mask==False]=True
labels,nlabels = ndimage.label(centers)
print " \n", "Initially,",nlabels, "clusters, including the background.", "\n"
sizes = ndimage.sum(centers,labels,index=range(0,nlabels))
for i,s in enumerate(sizes): #only keep little clusters, the center triangles
    if s > 500.:
        centers[labels==i] = False
io.imsave('output/4centers.png',centers)


# Output the approximate values from the program calculations
labels,nlabels = ndimage.label(centers)
print " \n", "There are", nlabels, "nanostructures."
print "And on average,", int(filled/float(nlabels)), "pixels per nanostructure.","\n"


#	Manual count shows 1030 triangles present in image




 

