import numpy
import scipy
from scipy import misc
from scipy import ndimage
from pylab import *
from PIL import *

image =array(Image.open('zoom1.tif').convert('L'))
ga = ndimage.gaussian_filter(image,11)

fga=ndimage.gaussian_filter(ga,1)

alpha=30
sharp=ga + alpha * (ga-fga)


figure(figsize=(20,40))
subplot(231)
imshow(image)
subplot(232)
imshow(ga, cmap=gray())
subplot(233)
imshow(sharp, cmap=gray())


subplots_adjust(wspace=0.02, hspace=0.1, top=0.9, bottom=0, left=0,
                    right=1)

savefig('gz_hist1.jpg')



